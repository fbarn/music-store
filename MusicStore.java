package assignment4;
import java.util.ArrayList;

public class MusicStore {
        MyHashTable<String,Song> TitlesongTable;
        MyHashTable<String,ArrayList<Song>> ArtistsongTable;
        MyHashTable<Integer,ArrayList<Song>> YearsongTable;


    public MusicStore(ArrayList<Song> songs) {
        //ADD YOUR CODE BELOW HERE
        this.TitlesongTable = new MyHashTable<String,Song>(songs.size()*2);
        this.ArtistsongTable = new MyHashTable<String,ArrayList<Song>>(songs.size()*2);
        this.YearsongTable = new MyHashTable<Integer,ArrayList<Song>>(songs.size()*2);
	for (Song song: songs){
		this.addSong(song);
        }
        //ADD YOUR CODE ABOVE HERE
    }


    /**
     * Add Song s to this MusicStore
     */
    public void addSong(Song s) {
        // ADD CODE BELOW HERE
	ArrayList<Song> byArtist=new ArrayList<Song>();
	ArrayList<Song> byYear=new ArrayList<Song>();

	if(ArtistsongTable.get(s.getArtist())!=null){
		byArtist=ArtistsongTable.get(s.getArtist());
	}
	if(YearsongTable.get(s.getYear())!=null){
		byYear=YearsongTable.get(s.getYear());
	}

		byArtist.add(s);
		byYear.add(s);
	this.TitlesongTable.put(s.getTitle(), s);
	this.ArtistsongTable.put(s.getArtist(), byArtist);
	this.YearsongTable.put(s.getYear(), byYear);
        // ADD CODE ABOVE HERE
    }

    /**
     * Search this MusicStore for Song by title and return any one song
     * by that title
     */
    public Song searchByTitle(String title) {
        //ADD CODE BELOW HERE
        	return TitlesongTable.get(title);//remove
        //ADD CODE ABOVE HERE
    }

    /**
     * Search this MusicStore for song by `artist' and return an
     * ArrayList of all such Songs.
     */
    public ArrayList<Song> searchByArtist(String artist) {
        //ADD CODE BELOW HERE
	if(ArtistsongTable.get(artist)!=null){
		return ArtistsongTable.get(artist);//remove
    	}
	return new ArrayList<Song>();
        //ADD CODE ABOVE HERE
    }

    /**
     * Search this MusicSotre for all songs from a `year'
     *  and return an ArrayList of all such  Songs
     */
    public ArrayList<Song> searchByYear(Integer year) {
        //ADD CODE BELOW HERE
	if (YearsongTable.get(year)!=null){
		return YearsongTable.get(year);//remove
	}
	return new ArrayList<Song>();
        //ADD CODE ABOVE HERE

    }
}
