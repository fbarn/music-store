// Filippo Baronello
// 260869515
// 4 December 2018

package assignment4;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

public class MyHashTable<K,V> implements Iterable<HashPair<K,V>>{
    // num of entries to the table
    private int numEntries;
    // num of buckets
    private int numBuckets;
    // load factor needed to check for rehashing
    private static final double MAX_LOAD_FACTOR = 0.75;
    // ArrayList of buckets. Each bucket is a LinkedList of HashPair
    private ArrayList<LinkedList<HashPair<K,V>>> buckets;

    // constructor
    public MyHashTable(int initialCapacity) {
        // ADD YOUR CODE BELOW THIS
        if (initialCapacity <= 0){
		throw new IndexOutOfBoundsException();
	}
	this.numEntries=0;
	this.numBuckets=initialCapacity;
	this.buckets=new ArrayList<LinkedList<HashPair<K,V>>>();
	for (int i=0; i<initialCapacity; i++){
		this.buckets.add(new LinkedList<HashPair<K,V>>());
	}

        //ADD YOUR CODE ABOVE THIS
    }

    public int size() {
        return this.numEntries;
    }

    public int numBuckets() {
        return this.numBuckets;
    }

    /**
     * Returns the buckets vairable. Usefull for testing  purposes.
     */
    public ArrayList<LinkedList< HashPair<K,V> > > getBuckets(){
        return this.buckets;
    }
    /**
     * Given a key, return the bucket position for the key.
     */
    public int hashFunction(K key) {
        int hashValue = Math.abs(key.hashCode())%this.numBuckets;
        return hashValue;
    }
    /**
     * Takes a key and a value as input and adds the corresponding HashPair
     * to this HashTable. Expected average run time  O(1)
     */
    public V put(K key, V value) {
        //  ADD YOUR CODE BELOW HERE
	    if (key == null) {
		    throw new IllegalArgumentException();
	    }

	    LinkedList<HashPair<K,V>> entry=this.buckets.get(this.hashFunction(key));

	    for (HashPair<K,V> i : entry){
		    if ((i.getKey()).equals(key)){
			    V oldvalue=i.getValue();
			    i.setValue(value);
			    return oldvalue;
		    }
	    }
	    this.numEntries+=1;
	    entry.addLast(new HashPair<K,V>(key, value));
	    float _entries=this.numEntries;
	    float _buckets=this.numBuckets;
	    float loadfactor=_entries/_buckets;
	    if (loadfactor>MAX_LOAD_FACTOR){
		    this.rehash();
	    }
	    return null;

        //  ADD YOUR CODE ABOVE HERE
    }


    /**
     * Get the value corresponding to key. Expected average runtime = O(1)
     */

    public V get(K key) {
        //ADD YOUR CODE BELOW HERE

	    if (key==null){
		    return null;
	    }

	LinkedList<HashPair<K,V>> entry=this.buckets.get(this.hashFunction(key));
	for (HashPair<K,V> i : entry){
		if ((i.getKey()).equals(key)){
			return i.getValue();
		}
	}
        return null;//remove
        //ADD YOUR CODE ABOVE HERE
    }

    /**
     * Remove the HashPair correspoinding to key . Expected average runtime O(1)
     */
    public V remove(K key) {
        //ADD YOUR CODE BELOW HERE

	    if (key==null){
		    return null;
	    }

	LinkedList<HashPair<K,V>> entry=this.buckets.get(this.hashFunction(key));
	for (int i=0; i<entry.size(); i++){
		if ((entry.get(i).getKey()).equals(key)){
			V oldvalue=entry.get(i).getValue();
			entry.remove(i);
			numEntries-=1;
			return oldvalue;
		}
	}
        return null;//remove
        //ADD YOUR CODE ABOVE HERE
    }

    // Method to double the size of the hashtable if load factor increases
    // beyond MAX_LOAD_FACTOR.
    // Made public for ease of testing.

    public void rehash() {
        //ADD YOUR CODE BELOW HERE
	MyHashTable<K,V> newList=new MyHashTable<K,V>(this.numBuckets*2);
	MyHashIterator iter=this.iterator();
	while(iter.hasNext()){
		HashPair<K,V> j= iter.next();
		newList.put(j.getKey(), j.getValue());
	}
	this.buckets=newList.buckets;
	this.numBuckets=newList.numBuckets;
        //ADD YOUR CODE ABOVE HERE
    }

    /**
     * Return a list of all the keys present in this hashtable.
     */

    public ArrayList<K> keys() {
        //ADD YOUR CODE BELOW HERE
	ArrayList<K> _keys=new ArrayList<K>();
	MyHashIterator iter=this.iterator();
	while(iter.hasNext()){
		HashPair<K,V> j= iter.next();
		_keys.add(j.getKey());
	}
        return _keys;
        //ADD YOUR CODE ABOVE HERE
    }

    /**
     * Returns an ArrayList of unique values present in this hashtable.
     * Expected average runtime is O(n)
     */
    public ArrayList<V> values() {
        //ADD CODE BELOW HERE
	MyHashTable<V,V> newList=new MyHashTable<V,V>(this.numBuckets);
	ArrayList<V> _vals=new ArrayList<V>();
	MyHashIterator iter=this.iterator();
	while(iter.hasNext()){
		HashPair<K,V> j= iter.next();
		if (newList.put(j.getValue(), j.getValue())==null){
			_vals.add(j.getValue());
		}
	}
        return _vals;
        //ADD CODE ABOVE HERE
    }


    @Override
    public MyHashIterator iterator(){
        return new MyHashIterator();
    }


    private class MyHashIterator implements Iterator<HashPair<K,V>> {
        private LinkedList<HashPair<K,V>> entries;

        private MyHashIterator() {
            //ADD YOUR CODE BELOW HERE
		this.entries=new LinkedList<HashPair<K,V>>();
		for (LinkedList<HashPair<K,V>> i : MyHashTable.this.buckets){
			for (HashPair<K,V> j : i){
				this.entries.addLast(j);
			}
		}
            //ADD YOUR CODE ABOVE HERE
        }

        @Override
        public boolean hasNext() {
            //ADD YOUR CODE BELOW HERE
            return this.entries.size()>0;// remove
            //ADD YOUR CODE ABOVE HERE
        }

        @Override
        public HashPair<K,V> next() {
            //ADD YOUR CODE BELOW HERE
		if (!(this.hasNext())){
			throw new NoSuchElementException();
		}
		return this.entries.remove();//remove
            //ADD YOUR CODE ABOVE HERE
        }

    }
}
